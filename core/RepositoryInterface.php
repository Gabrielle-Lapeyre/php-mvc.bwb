<?php
namespace BWB\Framework\mvc;

/**
 * Cette interface fournie les méthodes pour récupérer une multitude d'objets
 */
interface RepositoryInterface {

    /**
     * Permet de récupérer toutes les données on ne met rien en argument, 
     * retourne le résultat sous forme d'objet
     */
    public function getAll() : Object;

    /**
     *  Permet de récupérer toutes les données selon un filtre passé en argument,
     *  retourne le résultat sous forme d'objet
     */
    public function getAllBy(array $filters) : Object;
}