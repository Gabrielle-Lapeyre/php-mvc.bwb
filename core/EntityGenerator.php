<?php
namespace BWB\Framework\mvc\generator;
use BWB\Framework\mvc\generator\DAOGenerator;
use PDO;

abstract class EntityGenerator
{

    private static $nom;

    /**
     * Je me connecte à la base de donnée et je récupère la table correspondante et les noms des colonnes
     */
    private function getColumnsNames()
    {
        $config = json_decode(file_get_contents("config/database.json"),true);
        $dsn = $config['driver'].":dbname=".$config['dbname'].";host=".$config['host'].";charset=".$config['charset'];
        $db = new PDO($dsn, $config['username'],$config['password']);

        $statement = $db->prepare("DESCRIBE ".EntityGenerator::$nom);
        $statement->execute();
        return $tableFields = $statement->fetchAll(PDO::FETCH_COLUMN);
    }

    /**
     * J'ouvre le fichier au chemin indiqué, si il n'existe pas le créé et place le curseur au début du fichier.
     * Ensuite j'écris ligne par ligne 
     */
    private function writeFile()
    {
        $fileName = ucfirst(EntityGenerator::$nom) . ".php";
        $path = $_SERVER['DOCUMENT_ROOT']."models/".$fileName; // Chemin du fichier à créer 
        $handle = fopen($path, 'c');
        fwrite($handle,"<?php \n");
        fwrite($handle,"namespace BWB\Framework\mvc\models; \n");
        fwrite($handle,"use BWB\Framework\mvc\EntityModel; \n");
        fwrite($handle,"use BWB\Framework\mvc\dao\\".ucfirst(EntityGenerator::$nom)."DAO; \n");
        fwrite($handle,"class ".ucfirst(EntityGenerator::$nom)." extends EntityModel { \n");
            fwrite($handle, EntityGenerator::createProperties());
            fwrite($handle, EntityGenerator::createConstruct());
            fwrite($handle, EntityGenerator::createGetters());
            fwrite($handle, EntityGenerator::createSetters());
        fwrite($handle,"}");   
        fclose($handle);
    }

    /**
     * Méthode de création des propriété selon les noms des colonnes
     */
    private function createProperties()
    {
        $properties = "";
        $columnsNames = EntityGenerator::getColumnsNames();
        foreach($columnsNames as $key=>$value){
            $properties = $properties."protected $".$value."; \n";
        }
        $properties = $properties."\n";
        return $properties;
    }

    /**
     * Méthode de création du construct
     */
    private function createConstruct()
    {
        $construct = 'public function __construct(){'."\n";
        $construct = $construct.'$this->dao = new '.ucfirst(EntityGenerator::$nom)."DAO(); \n";
        $construct = $construct.'}'."\n \n";

        return $construct;
    }

    /**
     * Méthode de création des getters
     */
    private function createGetters()
    {
        $getters = "";
        $columnsNames = EntityGenerator::getColumnsNames();
        foreach($columnsNames as $key=>$value){
            $getters = $getters."/** \n * Get the value of ".$value."\n */ \n";
            $getters = $getters."public function get".ucfirst($value)."() \n { \n";
                $getters = $getters.'return $this->'.$value."; \n } \n \n";
        }

        return $getters;
    }

    /**
     * Méthode de création des setters
     */
    private function createSetters()
    {
        $setters = "";
        $columnsNames = EntityGenerator::getColumnsNames();
        foreach($columnsNames as $key=>$value){
            $setters = $setters."/** \n * Set the value of ".$value."\n * \n * @return self \n */ \n";
            $setters = $setters."public function set".ucfirst($value)."($".$value.") \n { \n";
                $setters = $setters.'$this->'.$value.' = $'.$value."; \n";
                $setters = $setters.'return $this;'." \n } \n \n";
        }

        return $setters;
    }

    /**
     * Methode qui créera la Classe correspondant au paramètre passé en argument
     */
    static function generate(string $name){
        EntityGenerator::$nom = $name;
        // Appel des diverses méthodes private
        EntityGenerator::writeFile();
        DAOGenerator::generate($name);
    }

    /**
     * Methode qui créera les Classes correspondantes aux valeurs du tableau passé en argument
     */
    static function generateMultiple(array $names){
        foreach($names as $key=>$object){
            EntityGenerator::$nom = $object[0];
            // Appel des diverses méthodes private
            EntityGenerator::writeFile();
            DAOGenerator::generate($object[0]);
        }
    }
}
