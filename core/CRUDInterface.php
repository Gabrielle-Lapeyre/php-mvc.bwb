<?php 
namespace BWB\Framework\mvc;

/**
 * Cette interface fournie les méthodes du CRUD pour manipuler une entité
 */
interface CRUDInterface {

    /**
     * Permet de récupérer une entitée selon l'id passé en argument,
     * retourne cette entitée
     */
    public function retrieve(int $id) : Object;

    /**
     * Permet de modifier une entitée par les valeurs passées en argument,
     * retourne un boolean selon la reussite de l'opération
     */
    public function update(array $data) : bool;

    /**
     * Permet de supprimer une entité selon l'id passé en argument,
     *  retourne un boolean selon la reussite de l'opération
     */
    public function delete(int $id) : bool;

    /**
     * Permet de créer une entitée avec les valeurs entrées en argument,
     * retourne l'entitée crée
     */
    public function create(array $data) : Object;
}