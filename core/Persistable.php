<?php
namespace BWB\Framework\mvc;

/**
 * Cette interface fournit des méthodes 'simplifiée' de manipultation CRUD
 */
interface Persistable
{
    /**
     * Permet d'invoquer la méthode retrieve sur le DAO en passant en argument
     * l'id de l'entité courante
     */
    public function load();

    /**
     * Permet d'invoquer la méthode create sur le DAO si l'entité courante n'a pas d'id, 
     * et de l'update dans le cas contraire
     */
    public function update(array $data);

    /**
     * Permet d'invoquer la méthode delete du DAO
     */
    public function remove();
}