<?php
namespace BWB\Framework\mvc;

/**
 * Un objet sera instancié à chaque requête, son objectif est d'invoquer la méthode qui correspond à la requête
 */
class Routing
{
    /**
     * Représente le mapping du fichier config/routing.json
     * c'est un tableau associatif dont la clé est une URI
     * Elle est initialisée à la contruction de l'objet Routing
     */
    private $config;
    /**
     * Le découpage de l'URI en tableau
     */
    private $uri;
    /**
     * Le résultat du découpage de la route
     */
    private $route;
    /**
     * Correspond au contrôleur qui a été trouvé
     */
    private $controller;
    /**
     * Représente les éléments variables de l'URI
     */
    private $args;
    /**
     * Correspond au verbe http utilisé dans requete
     */
    private $method;

    /**
     * Le constructeur va initialiser la propriété $config en la peuplant des données
     * contenues dans le fichier routing.json
     */
    public function __construct()
    {
        if(is_null($this->config)){
            $this->config = json_decode(file_get_contents("./config/routing.json"), true);
        }  
        $this->args = array();
    }

    /**
     * Cette méthode déclenche le mécanisme de routage
     */
    public function execute()
    {
        $this->uri = explode("/", $_SERVER['REQUEST_URI']);
        $this->method = $_SERVER['REQUEST_METHOD'];             //Méthode de requête utilisée pour accéder à la page
        foreach($this->config as $key=>$value){
            $this->route = explode("/", $key);
            $this->controller = "BWB\\Framework\\mvc\\controllers\\".$this->getValue($value); 
            if($this->isEqual()){
                if($this->compare()){
                    break;
                }
            }
        }
    }
    /**
     * Compare la hauteur de deux tableaux
     */
    private function isEqual() : bool
    {
        if(count($this->uri) == count($this->route)){
            return true;
        }else{
            return false;
        }
    }
    /**
     * retourne le contrôleur correspondant à la route sélectionnée
     */
    private function getValue($value)
    {
        if(gettype($value) == 'string'){
            return $value;
        }elseif(is_array($value)){             
            // Je vérifie si la méthode demandé pour acceder existe dans le tableau $value
            if(isset($value[$this->method])){
                // Si elle existe j'appelle le controleur qui correspond
                return $value[$this->method];
            }else{
                return null;
            }
        }
    }

    /**
     * ajoute l’élément variable de l’URI si l’élément en cours est censé être variable
     */
    private function addArgument($index)
    {
        $argument = $this->uri[$index];
        $explode = explode("&",$argument);
        // foreach()

        array_push($this->args, $argument);
        return true;
    }

    /**
     * compare les éléments des deux tableaux (uri et route) si les deux tableaux correspondent, c’est que la route a été trouvée.
     */
    private function compare() 
    {
        for($i=0; $i < count($this->uri); $i++){
            if($this->uri[$i] != $this->route[$i]){
                if($this->route[$i] == '(:)'){
                    $this->addArgument($i);
                }else{
                    return false;
                }
            }
        }
        $this->invoke();
    }

    /**
     * Elle créée un objet contrôleur et invoque la méthode en y passant les arguments adéquats
     */
    private function invoke()
    {
        $controller = explode(":", $this->controller);
        
        $object = new $controller[0]();
        
        return call_user_func_array(array($object, $controller[1]), $this->args);
    }
}