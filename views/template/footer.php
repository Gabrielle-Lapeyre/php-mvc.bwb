        <footer>
            <!-- jQuery JS -->
            <script src="vendor/components/jquery/jquery.min.js"></script>
            <!-- Bootstrap JS -->
            <script src="/vendor/twbs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
            <!-- Theme JS -->
            <script src="/vendors/js/index.js"></script>
        </footer>
    </body>
</html>