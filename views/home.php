<?php
    include "template/header.php";
    include "template/navbar.php";
?>
<section class="pb_0 container">
    <div class="row">
        <table class="table">
            <thead>
                <tr>
                    <th>Date d'emprunt</th>
                    <th>Adherent</th>
                    <th>Livre emprunté</th>
                    <th>Date de retour</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($emprunts as $emprunt){
                    echo "<tr>";
                    echo "<td>".$emprunt['date_debut']."</td>";
                    echo "<td><a href='http://php-decouverte.bwb/exc-adherent?adh=".$emprunt['numero']."'>".$emprunt['numero']."</a></td>";
                    echo "<td>".$emprunt['titre']."</td>";
                    echo "<td>".$emprunt['date_fin']."</td>";
                    echo "</tr>";
                } 
                ?>
            </tbody>
        </table>
    </div>
    <div class="row mt-10 p-0">
        <!-- Nouvel Emprunt -->
        <div class="who_we_area col-md-4 col-sm-6 wow fadeInUp">
            <div class="service-1 purple-after btn-primary cursor-btn" data-toggle="modal" data-target="#newLoan">
                <div class="servise-top wow fadeInUp">
                    <img src="/vendors/images/add-rent63-variant.png">
                </div>
                <h2 class="unify"> NOUVEL  <br> EMPRUNT </h2>
            </div>
        </div>
        <!-- Chercher un adherent -->
        <div class="who_we_area col-md-4 col-sm-6  wow fadeInUp">
            <div class="service-1 purple-after btn-primary cursor-btn" data-toggle="modal" data-target="#searchAdh">
                <div class="servise-top wow fadeInUp">
                    <img src="/vendors/images/search-user63-variant.png">
                </div>
                <h2 class="unify"> CHERCHER <br>UN ADHERENT </h2>
            </div>
        </div>
        <!-- Chercher un livre -->
        <div class="who_we_area col-md-4 col-sm-6  wow fadeInUp">
            <div class="service-1 purple-after btn-primary cursor-btn" data-toggle="modal" data-target="#searchBook">
                <div class="servise-top wow fadeInUp">
                    <img src="/vendors/images/search-book63-variant.png">
                </div>
                <h2 class="unify"> CHERCHER <br>UN LIVRE </h2>
            </div>
        </div>
    </div>
</section>
<!-- Modals -->
<!-- Nouvel emprunt -->
<div class="modal fade" id="newLoan" tabindex="-1" role="dialog" aria-labelledby="newLoanLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newLoanLabel">Nouvel emprunt</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="exc-nouvelEmprunt">
                <div class="modal-body">
                    <label >Livre ISBN</label>
                    <input id="inputAddIsbn" name="fIsbn" type="text" required>
                </div>
                <div class="modal-body">
                    <label >N° Adherent</label>
                    <input id="inputAddAdh" name="fAdh" type="text" data-toggle="tooltip" title="Si non renseigné vous serez redirigé vers une page de création de compte">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                    <button type="submit" class="btn btn-primary" >Ajouter</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Chercher un adherent -->
<div class="modal fade" id="searchAdh" tabindex="-1" role="dialog" aria-labelledby="searchAdhLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header modal-title-size">
                <h5 class="modal-title" id="searchAdhLabel">Chercher un adherent</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>                        
            <form method="POST" action="exc-adherent">
                <div class="modal-body">
                    <label >Adherent</label>
                    <input id="inputSearchAdd" name="sAdh" type="text" required>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                    <button type="submit" class="btn btn-primary">Chercher</button>
                </div>
            </form>                  
        </div>
    </div>
</div>
<!-- Chercher un livre -->
<div class="modal fade" id="searchBook" tabindex="-1" role="dialog" aria-labelledby="searchBookLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="searchBookLabel">Chercher un livre</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="exc-livre">
            <div class="modal-body">
                    <label >Titre du livre</label>
                    <input id="inputSearchBook" name="sTitre" type="text" required>
            </div>
            <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                    <button type="submit" class="btn btn-primary">Chercher</button>
            </div>
            </form>  
        </div>
    </div>
</div>   
<?php
    include "template/footer.php";
?> 