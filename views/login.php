<?php
    include "template/header.php";
    include "template/navbar.php";
?>
<section class="pb_0 container">
    <form class="col-md-4 offset-md-4">
        <div class="row">
            <label>Identifiant</label>
            <input name="identifiant">
        </div>
        <div class="row">
            <label>Mot de Passe</label>
            <input name="mdp">
        </div>
        <div class="row">
            <button class="col-md-4 offset-md-8 btn btn-primary" type="submit">Login</button>
        </div>
    </form>
</section>