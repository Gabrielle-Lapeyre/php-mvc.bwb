<?php 
use BWB\Framework\mvc\generator\GeneratorChecker;
use BWB\Framework\mvc\generator\EntityGenerator;
use BWB\Framework\mvc\generator\DAOGenerator;
include "vendor/autoload.php";


echo "Ici vous avez la possibilité de créer automatiquement une classe Entité et la classe DAO concrète correspondate. \n";
echo "Les propriétés de la classe Entité sont créées à partir des colonnes de la table correspondante, assurez vous d'entrer un nom de table valide. \n";
echo "Vous avec la possibilité de: \n";
echo "      1. Créer tout les fichiers Entité et ConcretDAO correspondant à toutes les tables de la base de données. (all) \n";
echo "      2. Créer un fichier Entité et ConcretDAO correspondant à la table de la base de données demandée. (one nomTable) \n";

$flag = false;
while($flag == false){
    $asked = readline("Que voulez vous faire ? Entrez all ou one nomTable (exit pour sortir) : ");
    // Si l'utilisateur entre "one ..."
    if(substr($asked, 0, 3) == 'one'){
        // 2.

        // Je divise la réponse donnée par l'utilisateur pour récupérer le nom de la table
        $explode = explode(' ', $asked);
        $nameTable = strtolower($explode[1]);

        // Je vérifie que la table demandée existe dans la BDD 
        if(BWB\Framework\mvc\generator\GeneratorChecker::checkTableName($nameTable)){
            EntityGenerator::generate($nameTable);
            echo "Les fichiers ".ucfirst($nameTable).".php et ".ucfirst($nameTable)."DAO.php ont été créés. \n";
            $flag = true;
        } else{
            echo "Désolé, la table ".$nameTable." n'a pas été trouvée. \n";
        }
    }
    // Si l'utilisateur entre "all" 
    elseif(substr($asked, 0, 3) == 'all'){
        // 1.

        // Je récupère les noms des tables de la base de donnée et génère la création selon le nombre de tables
        $tablesNames = BWB\Framework\mvc\generator\GeneratorChecker::getTablesNames();
        EntityGenerator::generateMultiple($tablesNames);
        echo "Tous les fichiers ont été créés. \n";
        $flag = true;
    }elseif($asked == "exit"){
        $flag = true;
    }else{
        echo "Je n'ai pas compris. \n";
    }
}